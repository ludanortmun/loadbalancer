#README
Aquí vamos a trabajar para el proyecto del balanceador de cargas. Por lo pronto (al menos hasta encontrar una solución más elegante y efectiva) utilizaremos este archivo para describir la estructura del proyecto.

Inicialmente tendremos la carpeta src (la que tendrá el código en el que estamos trabajando) y la carpeta tests (donde podemos definir las cargas de trabajo que utilizaremos para la ejecución del proyecto). Favor de generar todos los archivos ejecutables dentro de una carpeta "build" en el directorio actual en el que trabajan, y todos los archivos de salida en un directorio "output". Estas carpetas serán ignoradas por git. Consideren también agregar al archivo .gitignore los diferentes archivos generados por sus editores de texto o ambientes de ejecución.
