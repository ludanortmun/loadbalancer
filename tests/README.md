Este directorio contiene las diferentes cargas de trabajo que se utilizarán. Cada una de ellas debe tener su propio subdirectorio, conteniendo en él el código fuente (o el archivo ejecutable), el archivo .json con los parámetros de ejecución y, opcionalmente, uno o más archivos de entrada para el programa.

Agregué un archivo llamado "example.json" para que se den una idea de cómo debe estructurarse. Este cambiará conforme se realicen cambios al código para reflejarlos.
